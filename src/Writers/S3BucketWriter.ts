import { S3 } from 'aws-sdk';
import { PutObjectRequest, Body } from 'aws-sdk/clients/s3';

import { Writer } from './Writer';

export class S3BucketWriter implements Writer
{
	private readonly client: S3;
	private readonly bucketName: string;

	public constructor(client: S3, bucketName: string)
	{
		this.client = client;
		this.bucketName = bucketName;
	}

	public write(fileName: string, data: Body): Promise<string>
	{
		return new Promise((resolve, reject) =>
			this.client.upload(this.createPutObjectRequest(fileName, data), ((error, data) =>
				error ? reject(`Error: ${error}`) : resolve(data.Location)
			))
		);
	}

	private createPutObjectRequest(fileName: string, data: Body): PutObjectRequest
	{
		return {
			Bucket: this.bucketName,
			Body: data,
			Key: fileName,
			ContentType: 'text/html'
		};
	}
}
