export interface Writer
{
	write: (fileName: string, data: any) => Promise<any>;
}
