import * as path from 'path';

import { Writer } from './Writer';
import { Fs } from '../Tools';

export class FileSystemWriter implements Writer
{
	private client: Fs;
	private readonly folderPath: string;

	public constructor(client: Fs, folderPath: string)
	{
		this.client = client;
		this.folderPath = folderPath;
	}

	public write(fileName: string, data: any): Promise<void>
	{
		return this.client.write(path.join(this.folderPath, fileName), data);
	}
}
