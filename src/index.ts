import { Factory } from './Factory';
import { affiliates } from './Configurations/Affiliates';

const factory = new Factory();
const webcamReader = factory.createWebcamReader();

async function createAffiliatesPages()
{
	const webcamsInformation = await webcamReader.read();

	for (let i = 0; i < affiliates.length; i++) {
		const indexPageProjector = factory.createIndexPageProjector(affiliates[i]);
		const indexPageWriter = factory.createIndexPageWriter(affiliates[i]);

		await indexPageWriter.write('index.html', indexPageProjector.project(webcamsInformation));
	}
}

createAffiliatesPages()
	.then(() => console.log('index.html for all affiliates generated'))
	.catch((e) => console.error('Error: ', e));
