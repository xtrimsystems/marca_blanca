import { HttpClient } from '../Http';

import { WebcamReader } from './WebcamReader';
import { WebcamInformation } from './WebcamInformation';

// If we have a "stagin system", we should move this to environment variables
const WEBCAMS_API = 'http://webcams.cumlouder.com/feed/webcams/online/61/1/';

export class HttpWebcamReader implements WebcamReader
{
	private client: HttpClient<WebcamInformation[]>;

	public constructor(client: HttpClient<WebcamInformation[]>)
	{
		this.client = client;
	}

	public async read(): Promise<WebcamInformation[]>
	{
		return this.client.getJson(WEBCAMS_API);
	}
}
