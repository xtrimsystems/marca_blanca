export interface WebcamInformation
{
	readonly wbmerNick: string;
	readonly wbmerThumb1: string;
	readonly wbmerPermalink: string;
	readonly wbmerId: string;
}
