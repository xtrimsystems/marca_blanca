import { WebcamInformation } from './index';

export interface WebcamReader
{
	read(): Promise<WebcamInformation[]>
}
