import { WebcamInformation } from './WebcamInformation';
import { WebcamReader } from './WebcamReader';
import { Fs } from '../Tools';

const JSON_FILE_WEBCAMS = 'webcams.json';

export class JsonFileWebcamReader implements WebcamReader
{
	private fs: Fs;

	public constructor(fs: Fs)
	{
		this.fs = fs;
	}

	public async read(): Promise<WebcamInformation[]>
	{
		return this.fs.read(JSON_FILE_WEBCAMS).then((data) => JSON.parse(data));
	}
}
