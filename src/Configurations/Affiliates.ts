import * as affiliatesJson from './affiliates.json';

export interface Affiliate
{
	name: string;
	url: string;
	natsWebcams: string;
	natsWebsite: string;
	assetsFolder: string;
	analyticsCode: string;
}

export const affiliates: Affiliate[] = affiliatesJson;
