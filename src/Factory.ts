import * as AWS from 'aws-sdk';
import * as path from 'path';

import { Affiliate } from './Configurations/Affiliates';
import { HttpClient } from './Http';
import {
	AnalyticsProjector,
	BigThumbnailProjector,
	BoxHeaderProjector,
	HeaderProjector,
	IndexPageProjector,
	SmallThumbnailProjector
} from './Projectors';
import { Fs } from './Tools';
import { HttpWebcamReader, JsonFileWebcamReader, WebcamInformation, WebcamReader } from './Webcams';
import { FileSystemWriter, S3BucketWriter, Writer } from './Writers';

export class Factory
{
	private fs: Fs | undefined;
	private isProduction: boolean = false;

	public createWebcamReader(): WebcamReader
	{
		if (this.isProduction) {
			return new HttpWebcamReader(new HttpClient<WebcamInformation[]>())
		}

		return new JsonFileWebcamReader(this.getFs());
	}

	public createIndexPageProjector(affiliate: Affiliate): IndexPageProjector
	{
		return new IndexPageProjector(
			affiliate,
			new HeaderProjector(),
			new BoxHeaderProjector(),
			new BigThumbnailProjector(),
			new SmallThumbnailProjector(),
			new AnalyticsProjector()
		);
	}

	public createIndexPageWriter(affiliate: Affiliate): Writer
	{
		const assetsFolder = affiliate.assetsFolder;

		if (this.isProduction) {
			return new S3BucketWriter(this.createS3Client(), assetsFolder)
		}

		return new FileSystemWriter(this.getFs(), path.join('./affiliates', assetsFolder));
	}

	private getFs(): Fs
	{
		if (this.fs === undefined) {
			this.fs = new Fs();
		}

		return this.fs;
	}

	/**
	 * Set the credentials: https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/loading-node-credentials-shared.html
	 * In ~/.aws/credentials add
	 *
		[default]
		aws_access_key_id = <YOUR_ACCESS_KEY_ID>
		aws_secret_access_key = <YOUR_SECRET_ACCESS_KEY>
	 * AWS SDK automatically reads from this file to be setup
	 */
	private createS3Client(): AWS.S3
	{
		AWS.config.update({region: 'us-west-2'});

		return new AWS.S3({
			apiVersion: 'latest'
		})
	}
}
