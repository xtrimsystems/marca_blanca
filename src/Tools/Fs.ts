import { promises as fsPromised } from 'fs';

export class Fs
{
	public async write(path: string, data: any): Promise<void>
	{
		return fsPromised.writeFile(path, data);
	}

	public async read(urlPath: string): Promise<string>
	{
		return fsPromised.readFile(urlPath, 'utf8');
	}
}
