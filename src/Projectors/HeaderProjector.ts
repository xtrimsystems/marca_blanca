import { Projector } from './Projector';
import { HeaderData } from './DataTypes/HeaderData';

export class HeaderProjector implements Projector
{
	public project(data: HeaderData): string
	{
		return `
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>${data.nameWebsite}</title>

	<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen,projection">
	<link rel="stylesheet" href="css/estilos.css" type="text/css" media="screen,projection">
	<link rel="stylesheet" href="css/modal.css" type="text/css" media="screen,projection">

</head>`;
	}
}
