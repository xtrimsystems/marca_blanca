import { BigThumbnailProjector } from './BigThumbnailProjector';
import { BoxHeaderProjector } from './BoxHeaderProjector';
import { HeaderProjector } from './HeaderProjector';
import { Projector } from './Projector';
import { SmallThumbnailProjector } from './SmallThumbnailProjector';
import { WebcamInformation } from '../Webcams';
import { Affiliate } from '../Configurations/Affiliates';
import { AnalyticsProjector } from './AnalyticsProjector';

export class IndexPageProjector implements Projector
{
	private readonly affiliate: Affiliate;
	private headerProjector: HeaderProjector;
	private boxHeaderProjector: BoxHeaderProjector;
	private bigThumbnailProjector: BigThumbnailProjector;
	private smallThumbnailProjector: SmallThumbnailProjector;
	private analyticsProjector: AnalyticsProjector;

	public constructor(
		affiliate: Affiliate,
		headerProjector: HeaderProjector,
		boxHeaderProjector: BoxHeaderProjector,
		bigThumbnailProjector: BigThumbnailProjector,
		smallThumbnailProjector: SmallThumbnailProjector,
		analyticsProjector: AnalyticsProjector
	) {
		this.affiliate = affiliate;
		this.headerProjector = headerProjector;
		this.boxHeaderProjector = boxHeaderProjector;
		this.bigThumbnailProjector = bigThumbnailProjector;
		this.smallThumbnailProjector = smallThumbnailProjector;
		this.analyticsProjector = analyticsProjector;
	}

	public project(webcamsInformation: WebcamInformation[]): string
	{
		return `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="es" xmlns="http://www.w3.org/1999/xhtml">
${this.headerProjector.project({ nameWebsite: this.affiliate.name})}
${this.analyticsProjector.project({ code: this.affiliate.analyticsCode})}
<body>
	${this.boxHeaderProjector.project({nameWebsite: this.affiliate.name, natsWebsite: this.affiliate.natsWebsite})}
	<div class="listado-chicas">
		
		${this.getBigThumbnailsList(webcamsInformation)}
		
		${this.getSmallThumbnailsList(webcamsInformation)}
		
	</div>

	<a class="btn-mas-modelos" href="#" title="Mostrar más modelos">Siguiente Página</a>
	
	<div class="box-footer">
		<div class="menu">
			<a href="#" title="Acceso a las Chicas en Directo">Acceso a las Chicas en Directo</a> <span>|</span>
			<a href="#" title="Acceso Miembros">Acceso Miembros</a> <span>|</span>
			<a href="#" title="Compra Créditos">Compra Créditos</a>
		</div>
	</div>

	<div class="box-copy">
		<div class="menu">
			<p>Copyright © WAMCash Spain Todos los derechos reservados <span>|</span> <a href="#" title="Webmasters">Webmasters</a> </p>
			<p>Contenido para adultos <span>|</span> Tienes que tener mas de 18 años para poder visitarlo. Todas las modelos de esta web son mayores de edad.</p>
		</div>
	</div>

	<div class="box-data">
		<div class="menu">
			<a href="#" title="Soporte Epoch">Soporte Epoch</a> <span>|</span>
			<a href="#" title="18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement">18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement</a> <span>|</span>
			<a href="#" title="Contacto">Contacto</a> <span>|</span>
			<a href="#" title="Please visit Epoch.com, our authorized sales agent">Please visit Epoch.com, our authorized sales agent</a>
		</div>
	</div>

	<div class="modal isHidden">
		<button class="close">x</button>
		<div class="clear"></div>
		<iframe class="iframe" name="iframe"></iframe>
	</div>
	
	<script type="application/javascript" src="index.js"></script>
</body>
</html>`;
	}

	private getBigThumbnailsList(webcamsInformation: WebcamInformation[]): string
	{
		let html = '';

		if (webcamsInformation.length > 4) {
			for (let i = 0; i < 5; i++) {
				const webcam = webcamsInformation[i];

				html += this.bigThumbnailProjector.project({
					link: webcam.wbmerPermalink,
					image: webcam.wbmerThumb1,
					nick: webcam.wbmerNick,
					id: webcam.wbmerId,
					nats: this.affiliate.natsWebcams,
				});
			}
		}

		return html;
	}

	private getSmallThumbnailsList(webcamsInformation: WebcamInformation[]): string
	{
		let html = '';

		if (webcamsInformation.length > 1) {
			for (let i = 1; i < webcamsInformation.length; i++) {
				const webcam = webcamsInformation[i];

				html += this.smallThumbnailProjector.project({
					link: webcam.wbmerPermalink,
					image: webcam.wbmerThumb1,
					nick: webcam.wbmerNick,
					id: webcam.wbmerId,
					nats: this.affiliate.natsWebcams,
				});
			}
		}

		return html;
	}
}
