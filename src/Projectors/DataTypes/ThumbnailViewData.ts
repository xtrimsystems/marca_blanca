export interface ThumbnailViewData
{
	link: string;
	image: string;
	nick: string;
	nats: string;
	id: string;
}
