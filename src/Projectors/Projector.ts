export interface Projector
{
	project(data: any): string;
}
