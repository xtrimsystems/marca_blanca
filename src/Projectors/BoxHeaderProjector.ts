import { Projector } from './Projector';
import { BoxHeaderData } from './DataTypes/BoxHeaderData';

export class BoxHeaderProjector implements Projector
{
	public project(data: BoxHeaderData): string
	{
		return `
<div class="box-header">
	<div class="header">
		<h1 class="logo-sitio"><a href="#" title="${data.nameWebsite}">${data.nameWebsite}</a></h1>
		<div class="tit-webcams">Webcams</div>
	
		<div class="logo-cum">
			<a href="https://cumlouder.com?nats=${data.natsWebsite}" title="Cumlouder.com">Cumlouder.com</a>
		</div>
	
		<div class="menu">
		<a href="#" title="Acceso a las Chicas en Directo">Acceso a las Chicas en Directo</a> <span>|</span>
		<a href="#" title="Acceso Miembros">Acceso Miembros</a> <span>|</span>
		<a href="#" title="Compra Créditos">Compra Créditos</a>
		</div>
	
		<div class="clear"></div>
	</div>
</div>`;
	}
}
