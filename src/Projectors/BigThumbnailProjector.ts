import { Projector } from './Projector';
import { ThumbnailViewData } from './DataTypes/ThumbnailViewData';

export class BigThumbnailProjector implements Projector
{
	public project(data: ThumbnailViewData): string
	{
		return `
<div class="chica chica-grande">
	<a class="link" href="http://webcams.cumlouder.com/joinmb/cumlouder/${data.link}?nats=${data.nats}" target="iframe" title="${data.nick}">
		<span class="thumb">
			<img src="//w0.imgcm.com/modelos/${data.image}" alt="${data.nick}" title="${data.nick}" />
		</span>
		<span class="nombre-chica"><span class="ico-online"></span>${data.nick}</span>
		<span id="favorito" class="ico-favorito"></span>
	</a>
</div>
		`;
	}
}
