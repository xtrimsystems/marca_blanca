import Axios, { AxiosResponse } from 'axios';

export class HttpClient<T>
{
	public getJson(urlPath: string): Promise<T>
	{
		return this.get(urlPath).then((response) => response.data);
	}

	private get(urlPath: string): Promise<AxiosResponse<T>>
	{
		return Axios.get(urlPath);
	}
}
