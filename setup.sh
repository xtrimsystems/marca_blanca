#!/usr/bin/env bash

# Exit if a command exits with a non-zero status
set -e

USER_UID=$(id -u)
USER_GID=$(id -g)

printf "USER_UID=%s\nUSER_GID=%s\n" "$USER_UID" "$USER_GID" > .env

docker-compose build
