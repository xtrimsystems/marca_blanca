# Marca Blanca
**Disclaimers:**

- I've approach the task from an architectural point of view and focusing on the backend, I give for granted that the "affiliates" folders would exist. Even though I've created the JS and modify the css. This should be in another service, creating this folders and adding the assets when, for example, adding a new affiliate to the system. The "affiliates" folders here are just for development reasons.
- I will speak later about AWS and its products, because it is what I have been using far now, but all this can also be done with other vendors or by other means.

### Architecture

##### Description
Run a cronjob/script every 15 minutes that:

- Collects affiliates information (from a configuration file, or from an  API, ...)
- Calls Cumlouder API to get information of the webcams
- With the information collected, generates the affiliates index pages (index.html)
- Stores the index pages somewhere http publicly available (server, bucket, cdn, ...) where we have already the css and js.

Finally, as the task describes, our affiliates needs to point their DNS to where we put the index pages

##### Discarded Ideas
One ugly solution could be to have a server handling requests, and depending on the domain figuring out which affiliate is, then collecting relevant
information and generating and returning the index page. We could cache the response and wipe it every 15 minutes, but feels like a hacky solution.

A better possible solution could be to have a server or container serving static files,
and with a cronjob running a script/task to prepare them. This looks more like what I have describe in the description, but we can do better.

##### Final Implementation
The ideas described above are expensive and requires maintenance, since we are serving static files we can have something more simple. 

Have the static files (css, js, assets, ...) in a bucket (first point of the disclaimer section)

Have a lambda function that will generate the index pages and store them in the buckets.

With this approach we reduce our architecture to 0 maintenance and to look as simple as:
- Create a S3Bucket for each affiliate (it can be automated if we have them in the system). Affiliates need to point their DNS to their bucket.
- Create a lambda function that will run our script (one time thing)
- Set Amazon CloudWatch Events with Schedule Expressions using rate of 15 minutes to trigger the lambda (one time thing),
and maybe also a hook as a reaction of adding a new affiliate to our systems.

And previously I spoke about the other ideas being more expensive, it is because with the new approach
Amazon only charges by the usage of the tools; being less than 30 seconds for the lambda function every 15min,
and only for the network usage of the buckets. The solution should be more than enough given the load described in the task,
and if bigger we could always move to CloudFront with this setup.

##### Implementation Detail
(This part is intended to be read while or after checking the code, to answer to question that might appear)

For the reasons above I decided to implement the solution in node, so we can run it on a lambda function. I could have written
the same on PHP but is not natively supported in lambda functions(it needs to be compiled), or python that is supported but I have it a bit more rusty, same goes for java.

The solution is written in Typescript. Since some years already, js is dead for me. Finally we have a language for the frontend. End of the statement.

I'm not using any databases because I don't feel the need for it. As suggested in the task, the affiliate information is on
a configuration file for the sake of simplicity. On a real scenario we would want to have a service providing this information.

The task felt a bit simple and I wanted to show some techniques in bigger applications so I decided to write clean code (See Uncle Bob) 
and being explicit (just checking index.ts one knows what is going on).

Having interfaces, using a Factory (as a builder, but because having actual builders for this simple task felt over-engineering), readers and writers, projectors.

Wrapping the 3rd party libraries with own API's, like the HttpClient(axios) and the Fs, to abstract this from the rest of the application and business logic. 

I could have written a "simpler" and straight forward solution, in a single JS file, but would have been very imperative code, prone to errors, difficult to maintain and test.
And probably not even me in a couple of months would remember what is going on. That isn't how I code, I'm a professional and thus I write clean code.

The idea of the Projectors comes from when using event sourcing. The projectors task is to have ready the information (json, html, what-ever) that  
other services might need in the way they need it; instead of generating this one in the moment. For example generating an invoice. You don't need to wait
for the generation of the invoice after the customer finish the payment. It is better to redirect already the customer to a success page and send 
an event "generate_invoice" to a queue that when processed would send the necessary information to a Projector that will generate the invoice.

For the API calls or for saving the index pages we would want to do it on the correct system depending on what environment we are.
Meaning that while on production we would want to use the production cumlouder api, and store the generated index pages in AWS buckets; but on development,
for simplicity, we would not have those systems, so we can use the file system for example. To achieve this, the factory, or in a more complex scenario a builder,
will give us the correct class depending if we are on production or on development (See ./Factory, ./src/Webcams and ./src/Writers folders).

To test the call to Cumlouder API instead of reading from the file system (webcams.json files) please modify the factory's
method "createWebcamReader", for example to only return the HttpWebcamReader without checking any if. 
Dont' forget to run after changing code "docker-compose up -d --build lambda" to regenerate the index pages. 

Trying to test the S3BucketWriter will hang if no ~/.aws/credentials file exists on the system, and adding it will throw anyway an error for invalid arguments. 

Check any estilos.css to see how I easily solved the layout for the girls in the page. Spoiler alert: using css grid. 
No system needs to solve that problem, we just render the first 5 girls with BigThumbnailProjector and after that all the girls but
the 1st one with SmallThumbnailProjector, then css grid will take care of positioning all as we need.

The girls join page are loaded in the iframe using a simple and native way. Adding to the iframe: name="myIframe"
and then in the anchor links: target="myIframe". Then the links load the content inside the iframe. To open the modal that has the 
iframe we can listen to the onload method of the iframe and then open the modal. 
The modal can be close by pressing the button on the top right corner.

### Development / How to run
For local development you need docker.

To emulate the buckets we have a very simple ngnix setup serving static files base on the url. (See ./docker/ngnix folder).

To emulate the lambda function we have a node container where we can run the script to create the index.html (I discovered recently a
non official "docker lambda" container that claims to be exactly like AWS lambda functions, but I didn't have time to check it)

Both containers share the same volume, the ./affiliates/ folder, so the node script can write the index.html files and ngnix can serve them,
this emulates what on production would be a S3Bucket and a lambda writing in the S3Bucket.

Edit your hosts file. Add following line:
```
127.0.0.1       conejox.com cerdas.com babosas.biz
```
The first time we need to create the containers, run:
```
./setup.sh
```
Then, to start the containers run:
```
docker-compose up -d
```

Now we have running a container with ngnix serving our index.html files that got created with the node container, 
notice that we didn't have index.html files in the affiliates folders and now we have. To recreate this files run:
```
docker-compose up -d --build lambda
```
Running this every 15 minutes we could emulate the CloudWatch Events, but on development I don't think this is necessary.

To pause the containers run:
```
docker-compose stop
```
To remove the containers run:
```
docker-compose rm
```
